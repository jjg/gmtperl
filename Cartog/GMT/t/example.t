#! /usr/bin/perl -w

use POSIX;
use Test;

BEGIN { plan tests => 7, todo => [] }

require "./GMT.pm";

# A realistic test for GMT.pm
# This makes a labeled map of eastern England
#
# (c) J.J.Green 2001

# abbreviate some GMT commande line options

$range = "-R-0.5/1.5/53.2/54.7";
$proj  = "-JM6.5i";
$ticks = "-B1/1";
$res   = "-Df+";

$data     = "t/example";

$defs     = "$data/test.def";
$output   = "$data/test.eps";
$vlg      = "$data/vlg.txt";
$twn      = "$data/twn.txt";
$features = "$data/features.txt";

$common  = "$range $proj -P";

# set up the map

$map = new Map;

# check constructor

ok(defined $map);

$map->defaults($defs);
$map->output($output);
$map->verbose(1);

# check wrapper

skip(system("which GMT"), $map->wrapper("GMT")); 

# check access

ok($map->defaults(),$defs);
ok($map->output(),$output);
ok($map->verbose(),1);

# make the layers

$layers = $map->layers(new Layers);

ok(defined $layers);

# physical

$layers->pscoast("$common $res -Sc");
$layers->pscoast("-Q");
$layers->pscoast("$common $res -G240/255/240");
$layers->pscoast("$common $res -W1p/30/120/150");

# ocean features - we use the second argument to pass
# a perl function whose output is fed to psxy

sub mycat
{
    my ($stream,$file) = @_;

    unless (open FILE,"< $file")
    {
	warn "problem opening $file\n";
	return 0;
    }

    while (my $line = <FILE>)
    {
	chomp $line;
	print $stream "$line\n";
    }

    unless (close FILE)
    {
	warn "problem closing $file\n";
	return 0;
    }

    return 1;
}

$layers->pstext("$common -D0/0p",\&mycat,$features);

# Of course it is easier is do the above with either
# 
#   $layers->pstext("$features $common -D0/0p");
#
# or
#
#   $layers->pstext("$common -D0/0p","cat $features");
#
# but maybe your callback could generate data on the fly, etc 

# towns

$layers->pstext("$twn $common -D4p/0p");
$layers->psxy("$twn $common -Sc4p -W2");

# villages

$layers->pstext("$vlg $common -D3p/0p");
$layers->psxy("$vlg $common -Sc3p -W2");

# map furniture

$layers->psbasemap("$common $ticks");

# draw it

$map->draw();

# test

ok(-e $output);

# unlink $output;
unlink ".gmtcommands";









